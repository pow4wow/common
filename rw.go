package common

import (
	"context"
	"errors"
	"io"
	"net"
	"strings"
)

// WaitForMessage should be launched in routine
// It listens for incoming messages from connection
// and send it to the read channel (readChan)
func WaitForMessage(ctx context.Context, conn net.Conn, readChan chan<- []byte, errChan chan<- error, stopChan chan struct{}) {
	for {
		select {
		case <-ctx.Done():
			stopChan <- struct{}{}
			return
		default:
			in := make([]byte, 256)

			n, err := conn.Read(in)
			if err != nil {
				if err != io.EOF {
					errChan <- err
					return
				}

				continue
			}

			readChan <- in[:n]
		}
	}
}

// WriteToChannel should be launched in routine to be able to handle outgoing messages by writing it in connection (conn arg)
func WriteToChannel(ctx context.Context, conn net.Conn, writeChan <-chan []byte, errChan chan<- error, stopChan chan struct{}) {
	for {
		select {
		case <-ctx.Done():
			stopChan <- struct{}{}
			return
		case msg := <-writeChan:
			n, err := conn.Write(msg)
			if err != nil {
				errChan <- err
			}

			if n != len(msg) {
				errChan <- errors.New("message wasn't sent fully")
			}
		}
	}
}

// GetMessageValues returns message header and body, if it's satisfies the conditions
// or error if it doesn't
func GetMessageValues(msg []byte) (string, string, error) {
	if len(msg) == 0 {
		return "", "", errors.New("empty message received")
	}

	messageValue := string(msg)
	messageParts := strings.Split(messageValue, MessageSeparator)

	header := messageParts[0]
	if err := IsHeaderValid(header); err != nil {
		return "", "", err
	}

	message := ""

	if len(messageParts) > 1 {
		message = strings.Join(messageParts[1:], MessageSeparator)
	}

	return header, message, nil
}

// PrepareMessage returns message in appropriate format
func PrepareMessage(header string, message ...string) ([]byte, error) {
	if err := IsHeaderValid(header); err != nil {
		return nil, err
	}

	return []byte(strings.Join(append([]string{header}, message...), MessageSeparator)), nil
}

// IsHeaderValid checks if header can be accepted by server or client
func IsHeaderValid(header string) error {
	if _, ok := acceptableHeaders[header]; !ok {
		return errors.New("unsupported message header")
	}

	return nil
}
