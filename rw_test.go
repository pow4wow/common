package common

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsHeaderValid(t *testing.T) {
	t.Run("Valid header", func(t *testing.T) {
		assert.NoError(t, IsHeaderValid(ServerChallengeProvidedHeader))
	})

	t.Run("Unknown header", func(t *testing.T) {
		assert.Error(t, IsHeaderValid("some_unknown_value"))
	})
}

func TestPrepareMessage(t *testing.T) {
	type testCase struct {
		name           string
		header         string
		values         []string
		expectedResult string
		expectError    bool
	}

	var testCases = []testCase{
		{
			name:           "OK: Valid header with single value",
			header:         ClientChallengeResponseHeader,
			values:         []string{"foo"},
			expectedResult: "challenge-response:foo",
			expectError:    false,
		},
		{
			name:           "OK: Valid header with multiple values",
			header:         ClientRequestServiceHeader,
			values:         []string{"foo", "bar", "baz"},
			expectedResult: "request-service:foo:bar:baz",
			expectError:    false,
		},
		{
			name:           "MUST FAIL: Invalid header with multiple values",
			header:         "unknown-header",
			values:         []string{"foo", "bar", "baz"},
			expectedResult: "request-service:foo:bar:baz",
			expectError:    true,
		},
	}

	testCaseRunner := func(c testCase) func(t *testing.T) {
		return func(t *testing.T) {
			result, err := PrepareMessage(c.header, c.values...)
			if c.expectError {
				assert.Error(t, err)
				return
			}

			if !assert.NoError(t, err) {
				t.Fail()
				return
			}

			assert.Equal(t, c.expectedResult, string(result))
		}
	}

	for _, c := range testCases {
		t.Run(c.name, testCaseRunner(c))
	}
}

func TestGetMessageValues(t *testing.T) {
	type testCase struct {
		name           string
		message        []byte
		expectedHeader string
		expectedValue  string
		expectError    bool
	}

	var testCases = []testCase{
		{
			name:           "OK: Valid message",
			message:        []byte(fmt.Sprintf("%s:%s", ClientChallengeResponseHeader, "foo")),
			expectedHeader: ClientChallengeResponseHeader,
			expectedValue:  "foo",
			expectError:    false,
		},
		{
			name:           "MUST FAIL: Invalid message",
			message:        []byte(fmt.Sprintf("%s:%s", "unknown-header", "foo")),
			expectedHeader: "",
			expectedValue:  "",
			expectError:    true,
		},
	}

	testCaseRunner := func(c testCase) func(t *testing.T) {
		return func(t *testing.T) {
			header, value, err := GetMessageValues(c.message)
			if c.expectError {
				assert.Error(t, err)
				return
			}

			if !assert.NoError(t, err) {
				t.Fail()
				return
			}

			assert.Equal(t, c.expectedHeader, header)
			assert.Equal(t, c.expectedValue, value)
		}
	}

	for _, c := range testCases {
		t.Run(c.name, testCaseRunner(c))
	}
}
