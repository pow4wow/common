package common

// Client signals
const (
	ClientRequestServiceHeader    = "request-service"
	ClientChallengeResponseHeader = "challenge-response"
)

const (
	ServerChallengeProvidedHeader = "challenge-provided"
	ServerWordOfWisdomResponse    = "word-of-wisdom"
)

const (
	MessageSeparator = ":"
)

var (
	acceptableHeaders = map[string]struct{}{
		ClientChallengeResponseHeader: {},
		ClientRequestServiceHeader:    {},
		ServerChallengeProvidedHeader: {},
		ServerWordOfWisdomResponse:    {},
	}
)
